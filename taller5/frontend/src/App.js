import { BrowserRouter as Router, NavLink, Routes, Route, Navigate } from 'react-router-dom';
import './App.css';
import IntegrantesComponent from './views/IntegrantesComponent';
import RegistroAutosComponent from './views/form/RegistroAutosComponent';

function App() {
  function Navbar() {
    return (
      <div className="topnav">
        <NavLink to="/integrantes">Integrantes</NavLink>
        <NavLink to="/autos">Registro de Autos</NavLink>
    </div> 
    );
  }

  return (
    <Router>
      <Navbar />
      <Routes>
        <Route exact path="/" element={<Navigate replace to="/integrantes" />} />
        <Route path="/integrantes" element={<IntegrantesComponent />} />
        <Route path="/autos" element={<RegistroAutosComponent />} />
      </Routes>
    </Router>
  );
}

export default App;
