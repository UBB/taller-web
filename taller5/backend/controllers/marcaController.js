var Marca = require('../models/marca.js');

function list_marcas(req, res) {
    Marca.find({}, (err, marca) => {
        if (err) {
            return res.status(500).send({ message: 'Error: Could not get marcas!' });
        }
        res.status(200).send({ marca });
    })
}

function new_marca(req, res) {
    try{
        let marca = new Marca();
        marca.descripcion = req.body.descripcion;
        marca.save((err, marcaSave) => {
            if (err) {
                return res.status(400).send({ message: `Error: Could not save marca to database!> ${err}` });
            }
            res.status(200).send({ marca: marcaSave });
        })
    } 
    catch (error) {
        res.status(500).send({ message: `error: ` + error });
    }
}

function delete_marca(req, res) {
    let id = { '_id': req.params.id };
    Marca.deleteOne(id, (err, marca) => {
        if (err) {
            return res.status(400).send({ message: `Error: Could not delete marca from database!> ${err}` });
        }
        if (marca.deletedCount == 1) {
            res.status(200).send({ message: `Marca deleted!` });
        }
        else {
            res.status(400).send({ message: `Error: Marca could not be deleted!` });
        }
    });
}

module.exports = {
    list_marcas,
    new_marca,
    delete_marca
};
