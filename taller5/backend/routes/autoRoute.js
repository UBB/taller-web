'use strict'
var express = require('express');

var autoController = require('../controllers/autoController');

var api = express.Router();

api.get('/auto', autoController.list_autos);
api.post('/auto', autoController.new_auto);
api.delete('/auto/:id', autoController.delete_auto);

module.exports = api;
