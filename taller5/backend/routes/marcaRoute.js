'use strict'
var express = require('express');

var marcaController = require('../controllers/marcaController');

var api = express.Router();

api.get('/marca', marcaController.list_marcas);
api.post('/marca', marcaController.new_marca);
api.delete('/marca/:id', marcaController.delete_marca);

module.exports = api;
