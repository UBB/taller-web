import { BrowserRouter as Router, Routes, Route, Navigate } from 'react-router-dom';
import './App.css';
import IntegrantesComponent from './views/IntegrantesComponent';
import SignIn from './SignIn';
import Usuarios from './Usuarios';
import Navbar from './Navbar';

function App() {
  return (
    <Router>
      <Navbar />
      <Routes>
        <Route exact path="/" element={<Navigate replace to="/login" />} />
        <Route path="/integrantes" element={<IntegrantesComponent />} />
        <Route path="/usuarios" element={<Usuarios />} />
        <Route path="/login" element={<SignIn />} />
      </Routes>
    </Router>
  );
}

export default App;
