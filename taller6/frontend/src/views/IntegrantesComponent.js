import React, { Fragment } from 'react';
import { Container, List, ListItem, Typography } from '@material-ui/core';

const IntegrantesComponent = () => {
    return (
        <Fragment>
            <Container maxWidth="md">
                <List>
                    <ListItem>
                        <Typography variant="h2" gutterBottom>
                            Christopher Cromer
                        </Typography>
                    </ListItem>
                    <ListItem>
                        <Typography variant="h2" gutterBottom>
                            Benjamín Pérez
                        </Typography>
                    </ListItem>
                </List>
            </Container>
        </Fragment>
    );
};

export default IntegrantesComponent;
