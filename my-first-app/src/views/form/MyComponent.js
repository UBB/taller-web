import React,{Fragment,useState} from "react";

const MyComponent = () => {
    const[datos, setDatos] = useState("");
    const[people, setPeople] = useState([]);

    const handleInputChange = (event) => {
        setDatos({
            ...datos,
            [event.target.name] : event.target.value
        });
    };

    const sendDatos = () => {
        console.log("Sending datos First Name: " + datos.firstName + " and Last Name " + datos.lastName);

        let newPerson = {
            firstName: datos.firstName,
            lastName: datos.lastName
        }

        setPeople(people => [...people, newPerson]);
    }

    return (
        <Fragment>
            <h1>Form</h1>
            <div>
                <div>
                    <input type="text" placeholder="First Name" name="firstName" onChange={handleInputChange}></input>
                </div>
                <div>
                    <input type="text" placeholder="Last Name" name="lastName" onChange={handleInputChange}></input>
                </div>
                <button onClick={sendDatos} value="send">Send</button>
            </div>
            <div>
                {people.map((people) => (
                    <li>{people.firstName} {people.lastName}</li>
                ))}
            </div>
        </Fragment>
    );
};

export default MyComponent;
