'use strict'
var express = require('express');

var bookController = require('../controllers/bookController');

var api = express.Router();

api.get('/book', bookController.list_books);
api.get('/book/:id', bookController.show_book);
api.post('/book', bookController.new_book);
api.put('/book/:id', bookController.modify_book);
api.delete('/book/:id', bookController.delete_book);

module.exports = api;
