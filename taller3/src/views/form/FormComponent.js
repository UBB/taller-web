import React, { Fragment, useEffect, useState, useCallback } from 'react';
import axios from 'axios';
import MaterialDatatable from "material-datatable";
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import { Grid, Container, Box, Button, TextField, Typography } from '@material-ui/core';

const MySwal = withReactContent(Swal);

const FormComponent = () => {
    const grupo = 12;

    const apiUrl = "http://192.99.144.232:5000/api/personas";

    const[people, setPeople] = useState([]);

    const[id, setId] = useState("");
    const[nombre, setNombre] = useState("");
    const[apellido, setApellido] = useState("");

    const [guardarBtnDisabled, setGuardarBtnDisabled] = useState(true);
    const [eliminarBtnDisabled, setEliminarBtnDisabled] = useState(true);

    const changeButtonState = useCallback(() => { 
        setGuardarBtnDisabled(nombre === "" || apellido === "");
        setEliminarBtnDisabled(id === "");
    }, [nombre, apellido, id]);

    const handleInputChangeNombre = (event) => {
        setNombre(event.target.value);
    }

    useEffect(() => {
        changeButtonState();
    }, [changeButtonState, nombre]);

    useEffect(() => {
        changeButtonState();
    }, [changeButtonState, apellido]);

    useEffect(() => {
        changeButtonState();
    }, [changeButtonState, id]);

    const handleInputChangeApellido = (event) => {
        setApellido(event.target.value);
    }

    async function getPeople() {
        try {
            const response = await axios.get(apiUrl + '?grupo=' + grupo);
            if (response.status === 200) {
                setPeople(response.data.persona)
            }
        }
        catch (error) {
            console.error(error);
        }
    }

    function guardar() {
        if (id === "") {
            axios.post(apiUrl, {
                nombre: nombre,
                apellido: apellido,
                grupo: grupo
            })
            .then(function (response) {
                if (response.status === 200) {
                    MySwal.fire({
                        title: <p>Guardada!</p>,
                        text: 'La persona fue guardada!',
                        icon: 'success'
                    });
                    setId("");
                    setNombre("");
                    setApellido("");
                    getPeople();
                }
                else {
                    MySwal.fire({
                        title: <p>Error!</p>,
                        text: response.data.mensaje,
                        icon: 'error'
                    });
                }
            })
            .catch(function (error) {
                MySwal.fire({
                    title: <p>Error!</p>,
                    text: error.response.data.mensaje,
                    icon: 'error'
                });
            });
        }
        else {
            axios.put(apiUrl + '/' + id, {
                nombre: nombre,
                apellido: apellido,
                grupo: grupo
            })
            .then(function (response) {
                if (response.status === 200) {
                    MySwal.fire({
                        title: <p>Guardada!</p>,
                        text: 'La persona fue guardada!',
                        icon: 'success'
                    });
                    setId("");
                    setNombre("");
                    setApellido("");
                    getPeople();
                }
                else {
                    MySwal.fire({
                        title: <p>Error!</p>,
                        text: response.data.mensaje,
                        icon: 'error'
                    });
                }
            })
            .catch(function (error) {
                MySwal.fire({
                    title: <p>Error!</p>,
                    text: error.response.data.mensaje,
                    icon: 'error'
                });
            });
        }
    }

    function eliminar() {
        Swal.fire({
            title: 'Estás seguro?',
            text: "No se puede deshacer!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Eliminar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.isConfirmed) {
                axios.delete(apiUrl + '/' + id, {
                    nombre: nombre,
                    apellido: apellido,
                    grupo: grupo
                })
                .then(function (response) {
                    if (response.status === 200) {
                        MySwal.fire({
                            title: <p>Eliminada!</p>,
                            text: 'La persona fue eliminada!',
                            icon: 'success'
                        });
                        setId("");
                        setNombre("");
                        setApellido("");
                        getPeople();
                    }
                    else {
                        MySwal.fire({
                            title: <p>Error!</p>,
                            text: response.data.mensaje,
                            icon: 'error'
                        });
                    }
                })
                .catch(function (error) {
                    MySwal.fire({
                        title: <p>Error!</p>,
                        text: error.response.data.mensaje,
                        icon: 'error'
                    });
                });
            }
        });
    }

    useEffect(() => {
        getPeople();
    }, []);

    const columns = [
        {
            name: "Nombre",
            field: "nombre",
            options: {
                filter: true,
                sort: true,
            }
        },
        {
            name: "Apellido",
            field: "apellido",
            options: {
                filter: true,
                sort: true,
            }
        }
    ];

    const handleRowClick = (rowData, rowMeta) => {
        if (id === rowData._id) {
            setId("");
            setNombre("");
            setApellido("");
        }
        else {
            setId(rowData._id);
            setNombre(rowData.nombre);
            setApellido(rowData.apellido);
        }
    };

    const options = {
        filterType: 'checkbox',
        onlyOneRowCanBeSelected: true,
        onRowClick: handleRowClick
    };
    
    return (
        <Fragment>
            <Container maxWidth="md">
                <Grid container spacing={2}>
                    <Grid item xs={12} md={12}>
                        <Box mt={2}>
                            <Typography variant="h6">
                                Personas
                            </Typography>
                        </Box>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            id="nombre"
                            label="Nombre"
                            value={nombre}
                            onChange={handleInputChangeNombre}
                            variant="outlined"
                            fullWidth
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            id="apellido"
                            label="Apellido"
                            value={apellido}
                            onChange={handleInputChangeApellido}
                            variant="outlined"
                            fullWidth
                        />
                    </Grid>
                    <Grid item xs={12} sm={2}>
                        <Button disabled={guardarBtnDisabled} variant="contained" color="primary" fullWidth onClick={guardar}>
                            Guardar
                        </Button>
                    </Grid>
                    <Grid item xs={12} sm={2}>
                        <Button disabled={eliminarBtnDisabled} variant="contained" color="secondary" fullWidth onClick={eliminar}>
                            Eliminar
                        </Button>
                    </Grid>
                </Grid>

                <Grid item xs={12} md={12} className="material-table">
                    <Box mt={3}>
                        <MaterialDatatable
                            title="Personas"
                            columns={columns}
                            data={people}
                            options={options}
                        />
                    </Box>
                </Grid>
            </Container>
        </Fragment>
    );
};

export default FormComponent;
