import './App.css';
import { Route, BrowserRouter as Router, Switch, NavLink, Redirect } from 'react-router-dom'
import FormComponent from './views/form/FormComponent'
import IntegrantesComponent from './views/IntegrantesComponent'

function App() {
  function Navbar() {
    return (
      <div className="topnav">
        <NavLink to="/integrantes">Integrantes</NavLink>
        <NavLink to="/personas">Personas</NavLink>
    </div> 
    );
  }

  return (
    <Router>
      <Navbar />
      <Switch>
        <Route exact path="/">
          <Redirect to="/integrantes" />
        </Route>
        <Route path="/integrantes" component={IntegrantesComponent} />
        <Route path="/personas" component={FormComponent} />
      </Switch>
    </Router>
  );
}

export default App;
